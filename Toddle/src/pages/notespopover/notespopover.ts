import { Component } from '@angular/core';
import {
    IonicPage, NavController, NavParams, ViewController, ModalController, Events,
    LoadingController
} from 'ionic-angular';
import { SelectfruitPage } from '../selectfruit/selectfruit';
import { HotmealsnoPage } from '../hotmealsno/hotmealsno';
import * as moment from 'moment/moment';
import {TranslateService} from "@ngx-translate/core";
import {ProductServiceProvider} from "../../providers/product-service/product-service";
/**
 * Generated class for the NotespopoverPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-notespopover',
  templateUrl: 'notespopover.html',
})
export class NotespopoverPage {

  products :any;
  cateProdcuts :any;
  childData :any;
  constructor(public productService: ProductServiceProvider, public translate: TranslateService,public events: Events, public loading: LoadingController, public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController,public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
      console.log('ionViewDidLoad NotespopoverPage');
  }

  ionViewWillEnter() {
      console.log('ionViewWillEnter NotespopoverPage');
      this.products = this.navParams.data.products;
      this.childData = this.navParams.data.childData;
      var isEditItem = JSON.parse(localStorage.getItem('editItem'));

      if(this.cateProdcuts === undefined) {
          this.cateProdcuts = [];
          this.products.forEach(function(element) {
              if(element.category != undefined) {
                  var isCatThere = false;
                  for(let i= 0; i < this.cateProdcuts.length; i++) {
                      if(this.cateProdcuts[i].name === element.category) {
                          isCatThere = true;
                          break;
                      }
                  }

                  if(!isCatThere) {
                      var tempObj = {name : element.category, type: 'category', id:'', selected : false, multiple_possible: element.multiple_possible};
                      if(isEditItem != null && isEditItem != undefined) {
                            if(isEditItem.id == element.id) {
                                tempObj.id = element.product_id;
                                tempObj.selected = true;
                            }
                      }
                      this.cateProdcuts.push(tempObj);
                  }
              } else {
                  var tempElseObj = {name : element.name, type: 'product', id: element.id, selected : false, multiple_possible: element.multiple_possible};
                  if(isEditItem != null && isEditItem != undefined) {
                      if(isEditItem.id == element.id) {
                          tempElseObj.selected = true;
                      }
                  }

                  this.cateProdcuts.push(tempElseObj);
              }
          }.bind(this));
      }

      console.log(this.cateProdcuts);
  }

    dismiss(data) {
        this.viewCtrl.dismiss(data);
    }

    onProductChange(item) {
        this.cateProdcuts.forEach(function(element) {
            element.selected = false;
        });

        item.selected = true;
    }

    onSelectProduct() {
        let selectedItem = {type : '', name:'', multiple_possible : '', id:'', quantity : ''};
        this.cateProdcuts.forEach(function(element) {
            if(element.selected) {
                selectedItem = element;
            }
        }.bind(this));

        if(selectedItem.type === 'category') {
            let selProducts = [];
            this.products.forEach(function(element) {
                if(element.category == selectedItem.name) {
                    element.selected = false;
                    selProducts.push(element);
                }
            }.bind(this));

            let profileModal = this.modalCtrl.create(SelectfruitPage, {products : selProducts, mainProducts:this.navParams.data.products, childData: this.navParams.data.childData}, {showBackdrop : false});
            profileModal.present();
        } else {
            if(selectedItem.type !== '' && selectedItem.name !== '') {
                if(selectedItem.multiple_possible == '0') {
                    var regDatId = localStorage.getItem('regDayId');
                    var regId = localStorage.getItem('regId');

                    var isEditItem = JSON.parse(localStorage.getItem('editItem'));
                    var key = 'ADDING PRODUCT';
                    if(isEditItem != null) {
                        key = 'UPDATING PRODUCT';
                    }

                    let loader = this.loading.create({
                        content: this.translate.instant(key)
                    });
                    loader.present();

                    let productObj = {date: moment().format('YYYY-MM-DD'), product_id : selectedItem.id, quantity :'1', registration_day_id :regDatId, registration_id : regId}
                    let userDetails = JSON.parse(localStorage.getItem('userDetail'));

                    this.productService.saveUpdateProduct(userDetails , productObj).then((data) => {
                        loader.dismissAll();
                        this.closePopupsAndShowProduct();
                    },(data) => {
                        loader.dismissAll();
                        selectedItem.quantity = '1';
                        this.productService.setOfflineProducts(this.childData.child_id, selectedItem);
                        this.closePopupsAndShowProduct();
                    });
                } else {
                    let quantityModal = this.modalCtrl.create(HotmealsnoPage, {product: selectedItem, mainProducts:this.navParams.data.products, childData: this.navParams.data.childData});
                    quantityModal.present();
                }
            }
        }
    }

    closePopupsAndShowProduct () {
        let firstViewCtrl = this.navCtrl.first();
        this.navCtrl.popToRoot({animate: false}).then(() =>  {
            firstViewCtrl.dismiss();
            this.events.publish('productAdded');
        });
    }
}
