import { Component } from '@angular/core';
import {
    IonicPage, NavController, NavParams, ViewController, ModalController, Events,
    LoadingController
} from 'ionic-angular';
import {HotmealsnoPage} from "../hotmealsno/hotmealsno";
import * as moment from 'moment/moment';
import {TranslateService} from "@ngx-translate/core";
import {ProductServiceProvider} from "../../providers/product-service/product-service";

/**
 * Generated class for the SelectfruitPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-selectfruit',
  templateUrl: 'selectfruit.html',
})
export class SelectfruitPage {

  childData : any;
  fProdcuts : any;
  constructor(public productService: ProductServiceProvider, public translate: TranslateService,public events: Events, public loading: LoadingController, public navCtrl: NavController, public navParams: NavParams,public viewCtrl:ViewController,public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SelectfruitPage');
  }
   dismiss(data) {
    this.viewCtrl.dismiss(data);
  }

    ionViewWillEnter() {
        var isEditItem = JSON.parse(localStorage.getItem('editItem'));
        this.fProdcuts = this.navParams.data.products;
        this.childData = this.navParams.data.childData;
        console.log(this.fProdcuts);
        if(isEditItem != null && isEditItem != undefined) {
            this.fProdcuts.forEach(function (element) {
                if(element.id == isEditItem.id) {
                    element.selected = true;
                }
            })
        }
    }

    onFProductChange(item) {
        this.fProdcuts.forEach(function(element) {
            element.selected = false;
        });

        item.selected = true;
    }

    closePopupsAndShowProduct () {
        let firstViewCtrl = this.navCtrl.first();
        this.navCtrl.popToRoot({animate: false}).then(() =>  {
            firstViewCtrl.dismiss();
            this.events.publish('productAdded');
        });
    }

    onSelectFinalProduct() {
        let fSelectedItem;
        this.fProdcuts.forEach(function(element) {
            if(element.selected) {
                fSelectedItem = element;
            }
        }.bind(this));

        if(fSelectedItem !== undefined) {
            if(fSelectedItem.multiple_possible == '0') {
                var regDatId = localStorage.getItem('regDayId');
                var regId = localStorage.getItem('regId');
                var isEditItem = JSON.parse(localStorage.getItem('editItem'));
                var key = 'ADDING PRODUCT';
                if(isEditItem != null) {
                    key = 'UPDATING PRODUCT';
                }

                let loader = this.loading.create({
                    content: this.translate.instant(key)
                });
                loader.present();

                let productObj = {date: moment().format('YYYY-MM-DD'), product_id : fSelectedItem.id, quantity :'1', registration_day_id :regDatId, registration_id : regId}
                let userDetails = JSON.parse(localStorage.getItem('userDetail'));

                this.productService.saveUpdateProduct(userDetails , productObj).then((data) => {
                    loader.dismissAll();
                    this.closePopupsAndShowProduct();
                },(data) => {
                    loader.dismissAll();
                    let selectedItem = JSON.parse(JSON.stringify(fSelectedItem));
                    selectedItem.quantity = '1';
                    this.productService.setOfflineProducts(this.childData.child_id, selectedItem);
                    this.closePopupsAndShowProduct();
                });
            } else {
                let quantityModal = this.modalCtrl.create(HotmealsnoPage, {product: fSelectedItem, mainProducts:this.navParams.data.mainProducts, childData: this.navParams.data.childData});
                quantityModal.present();
            }
        }

    }
}
