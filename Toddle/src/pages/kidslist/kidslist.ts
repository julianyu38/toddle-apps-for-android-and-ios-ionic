import { Component, OnInit } from '@angular/core';
import childName from '../../data/childInfo';
import { Observable } from 'rxjs/Observable';
import "rxjs/add/observable/interval";
import {
    AlertController, LoadingController, MenuController, NavController, NavParams,
    PopoverController, Events
} from "ionic-angular";
import { ProfilePage } from "../profile/profile";
import { DetailsviewPage } from "../detailsview/detailsview";
import { KidsListServiceProvider } from "../../providers/kids-list-service/kids-list-service";
import { Storage } from "@ionic/storage";
import { LocationServiceProvider } from "../../providers/location-service/location-service";
import { LoginPage } from "../login/login";
import { PopoverContentPage } from "../popover/popover";
import { FiltersServiceProvider } from "../../providers/filters-service/filters-service";
import { Sqlite } from "../../providers/kids-database/sqlite";
import { Network } from '@ionic-native/network';
import * as moment from 'moment/moment';
import { Globals } from "../../app/globals";
import { TranslateService } from '@ngx-translate/core';
import { ProductServiceProvider } from "../../providers/product-service/product-service";
import { CommandHandler } from "../../providers/commandhandler";
import Global = NodeJS.Global;


@Component({
    selector: 'page-kidslist',
    templateUrl: 'kidslist.html',
})
export class KidslistPage implements OnInit {

    childName: { name: string, time: string, years: string, gender: string, avatar: string, icon: string }[];
    childList: any;
    filterChildList: any;
    isGrid: boolean;
    locationID: string;
    locationName: string;
    userDetails: { [k: string]: any } = {};
    myDate: any = new Date().toLocaleDateString();
    today: any;
    birth: any;
    user: any;
    cSearch = '';
    menuItems: any;
    syncTime: any;
    indexNum = 0;
    selLabels = [];

    constructor(private navCtrl: NavController,
        public navParams: NavParams,
        public kidList: KidsListServiceProvider,
        public alertCtrl: AlertController,
        public loading: LoadingController,
        public storage: Storage,
        public location: LocationServiceProvider,
        private menu: MenuController,
        public popoverCtrl: PopoverController,
        public kidsDb: Sqlite,
        public filterData: FiltersServiceProvider,
        public network: Network,
        public productService: ProductServiceProvider,
        public commandHandler: CommandHandler,
        public translate: TranslateService,
        public events: Events) {

        this.events.unsubscribe('filterApply');
        this.events.unsubscribe('syncCompleted');

        events.subscribe('filterApply', () => {
            if (this.childList != undefined) {
                this.childList.forEach(function (element) {
                    this.filterChildList.find(i => i.child_id === element.child_id ? element.selected = i.selected : false);
                }.bind(this));

                console.log(this.childList);
                this.filterChildList = this.filterData.filterData(this.childList);
                this.showSelectedFilterLabel();
            }
        });

        events.subscribe('syncCompleted', () => {
            this.doRefresh(undefined, false);
            Globals.SYNC_STARTED = false;
        });
    }


    showSelectedFilterLabel() {
        this.selLabels = this.filterData.getListOfSelectedLabels();
        console.log(this.selLabels);
    }

    onClickClear() {
        this.cSearch = '';
    }

    onInputChange() {
        console.log("onInputChange");
    }

    ionViewWillEnter() {
        if (localStorage.getItem('currentDate') == null) {
            localStorage.setItem('currentDate', moment().format('YYYY/MM/DD'));
        } else {
            if (new Date(moment().format('YYYY/MM/DD')) > new Date(localStorage.getItem('currentDate'))) {
                localStorage.removeItem('currentChild');
                localStorage.removeItem('childData');
                localStorage.removeItem('currentLocationId');
                localStorage.setItem('currentDate', moment().format('YYYY/MM/DD'));

                if (this.filterChildList !== undefined) {
                    this.filterChildList.forEach(function (element) {
                        element.selected = false;
                        element.visited = false;
                    })
                }
            }
        }

        if (localStorage.getItem('currentChild') != null && this.filterChildList != undefined) {
            var visitedChild = JSON.parse(localStorage.getItem('currentChild'));
            this.filterChildList.find(i => i.child_id == visitedChild.id ? i.selected = visitedChild.value : '');
        }

        this.userDetails.data = this.navParams.data.data;
        this.userDetails.installation_key = this.navParams.data.installation_key;

        if (this.childList !== undefined) {
            this.childList.forEach(function (element) {
                element.child_time = this.kidList.getChildHours(this.userDetails.data.user_id, element.child_id);
            }.bind(this));
        }

        this.showSelectedFilterLabel();

        // check for last sync hours and mins.
        if (JSON.parse(localStorage.getItem('syncQueue')) != null) {
            var lastSyncTime = localStorage.getItem('offlineStartTime');
            if (lastSyncTime != null) {
                var hm = this.kidList.getTimeDifference(new Date(Number(lastSyncTime)), new Date());
                if (hm.h > 0) {
                    this.syncTime = (hm.h + ' hours ' + hm.m + ' mins ago');
                } else {
                    this.syncTime = (hm.m + ' mins ago');
                }
            } else {
                this.syncTime = '';
            }
        } else {
            this.syncTime = '';
        }

    }

    //open popover to filter child list
    openPopover(myEvent) {
        var mLocationItems;
        this.menuItems.forEach(function (element) {
            if (String(element.id) == String(localStorage.getItem('currentLocationId'))) {
                mLocationItems = element;
            }
        });

        let popover = this.popoverCtrl.create(PopoverContentPage, mLocationItems);
        popover.present({
            ev: myEvent
        })

        popover.onDidDismiss(() => {
            // TO DO
        })
    }

    doRefresh(refresher, isRefresher) {
        if (this.network.type != 'none') {
            if (!isRefresher) this.commandHandler.showLoader('REFRESH_CONTENT');
            this.kidList.getKidsList(this.userDetails.data.user_id, this.userDetails.data.token,
                String(localStorage.getItem('currentLocationId')), this.userDetails.installation_key).then((data) => {
                    this.childList = data;
                    this.childList.forEach(function (element) {
                        element.child_time = this.kidList.getChildHours(this.userDetails.data.user_id, element.child_id);
                        element.selected = element.child_time.selected;
                        element.visited = element.child_time.visited;
                    }.bind(this));

                    this.filterChildList = this.childList;

                    if (localStorage.getItem('isDevice') === 'true') {
                        this.kidsDb.addKidsList(this.filterChildList, this.userDetails.installation_key, String(localStorage.getItem('currentLocationId')));
                    }

                    this.indexNum = 0;
                    this.getImagesData();

                    this.events.publish('filterApply');
                    if (isRefresher) {
                        refresher.complete();
                    } else {
                        this.navCtrl.popToRoot();
                    }
                });
        } else {
            if (isRefresher) refresher.complete();
        }

        setTimeout(function () {
            if (isRefresher) refresher.cancel();
        }, 10000);
    }


    //on view did enter
    ionViewDidEnter() {
        this.menu.enable(true, 'menu1');
        this.network.onDisconnect().subscribe(() => {
            console.log('network was disconnected :-(');
            Globals.IS_NETWORK_UP = false;
        });

        this.network.onConnect().subscribe((data) => {
            Globals.IS_NETWORK_UP = true;
            // We just got a connection but we need to wait briefly
            // before we determine the connection type. Might need to wait.
            // prior to doing any api requests as well.

            setTimeout(() => {
                console.log("network on " + data.type);
                if (this.network.type != 'none') {
                    console.log('we got a wifi connection, woohoo!');
                    if (!Globals.SYNC_STARTED) {
                        this.syncStartOnceNetworkUp();
                    }
                }
            }, 3000);
        });
    }

    setTime(){
        this.childList.forEach(function (element) {
            element.child_time = this.kidList.getChildHours(this.userDetails.data.user_id, element.child_id);
            element.selected = element.child_time.selected;
            element.visited = element.child_time.visited;
        }.bind(this));
    }

    syncStartOnceNetworkUp() {
        Globals.SYNC_STARTED = true;
        var syncData = JSON.parse(localStorage.getItem('syncQueue'));
        if (syncData != null && syncData.length > 0) {
            this.commandHandler.showLoader('Sync local data to server, Please wait for moment!');
            this.commandHandler.checkSyncToServer();
        } else {
            Globals.SYNC_STARTED = false;
        }
    }
    //on view did load
    ionViewDidLoad() {
        this.commandHandler.showLoader('LOADING');
        //get params
        this.userDetails.data = this.navParams.data.data;
        this.userDetails.installation_key = this.navParams.data.installation_key;
        //get child list from KidsListServiceProvider using api
        if (this.network.type != 'none') {
            //get location from LocationServiceProvider service using api
            this.location.getLocation(this.userDetails.data.user_id, this.userDetails.data.token,
                this.userDetails.installation_key).then((data) => {
                    if (data != null) {                        
                        Globals.IS_MULTIPLE_HOURS = (data[0].multiple_hours == 'true' || data[0].multiple_hours==true) ? true : false;
                        if (Globals.IS_MULTIPLE_HOURS) {
                            Globals.ATTEMPTS = 8;
                        } else {
                            Globals.ATTEMPTS = 2;
                        }

                        this.menuItems = data;
                        localStorage.setItem('locationData', JSON.stringify(data));
                        if (localStorage.getItem('isDevice') === 'true') {
                            this.kidsDb.addLocation(data, this.userDetails.installation_key).then((resp) => {
                                // To DO
                            })
                        }

                        this.menuItems.forEach(function (element) {
                            element.selected = false;
                        });
                        this.menuItems[0].selected = true;

                        this.locationName = this.menuItems[0].name;
                        localStorage.setItem('currentLocationId', String(this.menuItems[0].id));
                        //get child list from KidsListServiceProvider using api
                        this.kidList.getKidsList(this.userDetails.data.user_id, this.userDetails.data.token,
                            this.menuItems[0].id, this.userDetails.installation_key).then((data) => {
                                this.childList = data;
                                this.kidList.saveAllUserTimes(this.userDetails.data.user_id, this.childList);
                                var clock = Observable.interval(1000).map(() => {
                                    return true;
                                });

                                clock.subscribe((data)=>{
                                   this.setTime()
                                })

                                this.childList.forEach(function (element) {
                                    element.child_time = this.kidList.getChildHours(this.userDetails.data.user_id, element.child_id);
                                    element.selected = element.child_time.selected;
                                    element.visited = element.child_time.visited;
                                }.bind(this));

                                this.filterChildList = this.childList;
                                console.log(this.filterChildList);
                                if (localStorage.getItem('isDevice') === 'true') {
                                    this.kidsDb.addKidsList(this.childList, this.userDetails.installation_key, this.menuItems[this.locationID].id)
                                }
                                // Check for sync is updated or not!!!
                                this.syncStartOnceNetworkUp();
                                this.indexNum = 0;
                                this.getImagesData();
                            });
                    } else {
                        this.commandHandler.hideLoader();
                    }
                });
        } else {
            if (localStorage.getItem('isDevice') === 'true') {
                this.kidsDb.getLocations().then((data) => {

                    this.menuItems = JSON.parse(data[0].locations);

                    this.menuItems.forEach(function (element) {
                        element.selected = false;
                    });
                    this.menuItems[0].selected = true;

                    this.locationName = this.menuItems[0].name;
                    console.log(this.menuItems[0]);

                    Globals.IS_MULTIPLE_HOURS = (String(this.menuItems[0].multiple_hours) == 'true' || (this.menuItems[0].multiple_hours) == true) ? true : false;

                    if (Globals.IS_MULTIPLE_HOURS) {
                        Globals.ATTEMPTS = 8;
                    } else {
                        Globals.ATTEMPTS = 2;
                    }

                    localStorage.setItem('currentLocationId', String(this.menuItems[0].id));
                    this.kidsDb.getKidsListDb(this.menuItems[0].id).then((data) => {
                        this.childList = JSON.parse(data[0].kidList);
                        this.filterChildList = this.childList;
                        this.commandHandler.hideLoader();
                    });
                }, (data) => {
                    alert(this.translate.instant("Something went wrong! Please try again."));
                })
            }
        }
    }

    ngOnInit() {
        this.childName = childName;
        this.isGrid = true;
        this.locationID = "0";
    }
    //calculating child age from birth date
    calculateAge(date: string) {
        this.today = new Date(this.myDate);
        this.birth = new Date(date);
        var ms = this.today.getTime() - this.birth.getTime();
        var second = ms / 1000;
        var minutes = second / 60;
        var hours = minutes / 60;
        var days = hours / 24;
        var years = days / 365;
        return Math.round(years);
    }


    //navigation to child profile page
    goToProfile(name: {}) {
        this.navCtrl.push(ProfilePage, name);
    }

    // Get images for all the child once app will launch.
    getImagesData() {
        if (this.network.type != 'none') {
            if (this.indexNum < this.filterChildList.length) {
                var matchItem = Globals.CHILD_IMAGE_URLs.find(i => i.child_id == this.filterChildList[this.indexNum].child_id);
                if (matchItem !== undefined) {
                    this.filterChildList[this.indexNum].child_image_basedata = matchItem.data;
                    this.indexNum++;
                    this.getImagesData();
                } else {
                    if (this.filterChildList[this.indexNum].child_image_url != null && this.filterChildList[this.indexNum].child_image_url != '') {
                        this.productService.getPhotoFromUser(this.filterChildList[this.indexNum].child_image_url).then((data) => {
                            console.log('data:image/jpeg;base64');
                            this.filterChildList[this.indexNum].child_image_basedata = 'data:image/jpeg;base64,' + data;

                            // Store base64 data into global variables. while running the app and with the session, no need
                            // to call apis again once we get the data for particular id.
                            Globals.CHILD_IMAGE_URLs.push({ child_id: this.filterChildList[this.indexNum].child_id, data: this.filterChildList[this.indexNum].child_image_basedata })

                            this.indexNum++;
                            this.getImagesData();
                        }, (data) => {
                            this.filterChildList[this.indexNum].child_image_basedata = '';
                            this.indexNum++;
                            this.getImagesData();
                        });
                    } else {
                        this.filterChildList[this.indexNum].child_image_basedata = '';
                        this.indexNum++;
                        this.getImagesData();
                    }
                }
            } else {
                this.commandHandler.hideLoader();
            }
        } else {
            this.commandHandler.hideLoader();

            if (Globals.CHILD_IMAGE_URLs.length > 0) {
                this.filterChildList.forEach(function (element) {
                    var matchItem = Globals.CHILD_IMAGE_URLs.find(i => i.child_id == element.child_id);
                    if (matchItem !== undefined) {
                        element.child_image_basedata = matchItem.data;
                    }
                }.bind(this))
            }
        }
    }

    //navigation to child info page
    goDetailsView(name: string) {
        this.navCtrl.push(DetailsviewPage, name);
    }

    //change list view to grid or list view
    changeView(isGrid: boolean) {
        this.isGrid = isGrid;
    }

    updateLocalData(data) {
        this.locationID = data.id;
        this.locationName = data.name;
        localStorage.setItem('currentLocationId', String(data.id));

        this.menuItems.forEach(function (element) {
            if (element.id == data.id) {
                element.selected = true;
            } else {
                element.selected = false;
            }
        });

    }
    //get kids list by selected location from menu
    getKidListByLocation(locationObj) {
        if (String(localStorage.getItem('currentLocationId')) == String(locationObj.id)) {
            this.menu.close("menu1");
        } else {
            this.menu.close("menu1");
            this.commandHandler.showLoader('LOADING');
            if (this.network.type != 'none') {
                this.kidList.getKidsList(this.userDetails.data.user_id, this.userDetails.data.token,
                    locationObj.id, this.userDetails.installation_key).then((data) => {
                        this.childList = data;
                        var self = this;
                        Globals.IS_MULTIPLE_HOURS = (locationObj.multiple_hours == 'true' || locationObj.multiple_hours==true) ? true : false;
                        if (Globals.IS_MULTIPLE_HOURS) {
                            Globals.ATTEMPTS = 8;
                        } else {
                            Globals.ATTEMPTS = 2;
                        }

                        this.kidList.saveAllUserTimes(this.userDetails.data.user_id, this.childList);
                        this.childList.forEach(function (element) {
                            element.child_time = this.kidList.getChildHours(this.userDetails.data.user_id, element.child_id);
                            element.selected = element.child_time.selected;
                            element.visited = element.child_time.visited;
                        }.bind(this));

                        this.filterChildList = this.childList;

                        if (localStorage.getItem('isDevice') === 'true') {
                            this.kidsDb.addKidsList(this.childList, this.userDetails.installation_key, locationObj.id)
                        }

                        this.indexNum = 0;
                        this.getImagesData();
                        this.updateLocalData(locationObj);
                    });
            } else {
                if (localStorage.getItem('isDevice') === 'true') {
                    this.kidsDb.getKidsListDb(locationObj.id).then((data) => {
                        if (data) {
                            this.childList = JSON.parse(data[0].kidList);
                            this.filterChildList = this.childList;
                            this.updateLocalData(locationObj);
                        } else {
                            this.commandHandler.showAlert('Not Data Available', 'No data is available for this location. Please ON the internet to get the latest data!');
                        }
                        this.commandHandler.hideLoader();
                    }, (data) => {
                        this.commandHandler.showAlert('Error', 'Something went wrong, Please try again!');
                    });
                }
            }

            this.removeFilterLocalData();
        }

    }

    removeFilterLocalData() {
        this.selLabels = [];
        this.commandHandler.resetCommandStorage();
    }

    //logout
    logOut() {
        this.selLabels = [];
        this.commandHandler.goBackToLogin();
    }

    goToRegister(childData) {
        console.log(this.userDetails);
        console.log(childData);
        //if user try to update register time within a minute from last updated time
        if (childData.selected && (childData.child_time.h == 0 && childData.child_time.m < 1)) {
            let childInOuts = this.productService.getChildInOuts(this.userDetails.data.user_id, childData.child_id)[0]; 
            var hm = childInOuts.split(":");                      
            var min = Number(hm[1]);
            this.commandHandler.showAlert('Information', this.translate.instant('You can not deregister the child until after')+' '+ hm[0]+':'+((min+1)<10 ? '0'+(min+1):(min+1)));
        }
        else {
            var attemptsObj = this.kidList.saveUserAttempts(this.userDetails.data.user_id, childData.child_id, moment().format('HH:mm'));

            // > Globals.ATTEMPTS
            if (attemptsObj.attempts > Globals.ATTEMPTS) {
                let checkinTime = this.productService.getChildInOuts(this.userDetails.data.user_id, childData.child_id)[0];
                this.commandHandler.showAlert('Information', 'You have already done maximum In and Out for the day.');
            } else {

                if (childData.selected == undefined) childData.selected = true
                else if (childData.selected) childData.selected = false;
                else childData.selected = true;

                if (childData.selected == undefined) childData.visited = false
                else if (childData.selected) childData.visited = false;
                else childData.visited = true;

                var contentType = 'Loading';
                if (attemptsObj.loadingContent != '') contentType = attemptsObj.loadingContent;

                this.commandHandler.showLoader('LOADING');

                var childObj = {
                    "user_id": this.userDetails.data.user_id,
                    "attendance_detail": {
                        "child": {
                            "date": moment().format('YYYY-MM-DD'),
                            "child_id": childData.child_id,
                            "location_id": Number(localStorage.getItem('currentLocationId')),
                            "registration_id": childData.registration_id,
                            "registration_day_id": null
                        }
                    }
                };

                childObj.attendance_detail.child[attemptsObj.attr] = moment().format('HH:mm');
                console.log(childObj);

                this.kidList.doRegistrationOfChild(this.userDetails, childObj).then((data) => {
                    this.commandHandler.hideLoader();
                }, (data) => {
                    // TO DO
                });
            }
        }
    }
}

