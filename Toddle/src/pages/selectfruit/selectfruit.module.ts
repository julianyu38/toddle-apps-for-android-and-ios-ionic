import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelectfruitPage } from './selectfruit';

@NgModule({
  declarations: [
    SelectfruitPage,
  ],
  imports: [
    IonicPageModule.forChild(SelectfruitPage),
  ],
  exports: [
    SelectfruitPage
  ]
})
export class SelectfruitPageModule {}
