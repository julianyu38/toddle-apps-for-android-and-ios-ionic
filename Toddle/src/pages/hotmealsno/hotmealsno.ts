import { Component } from '@angular/core';
import {Events, IonicPage, LoadingController, NavController, NavParams, ViewController} from 'ionic-angular';
import {PickerController} from 'ionic-angular';
import {ProductServiceProvider} from "../../providers/product-service/product-service";
import * as moment from 'moment/moment';
import {TranslateService} from '@ngx-translate/core';
@IonicPage()
@Component({
  selector: 'page-hotmealsno',
  templateUrl: 'hotmealsno.html',
   
})

export class HotmealsnoPage {
  
    items = [{"name":"1","selected":false},{"name":"2","selected":false},{"name":"3","selected":false},{"name":"4","selected":false},{"name":"5","selected":false},{"name":"6","selected":false},{"name":"7","selected":false},{"name":"8","selected":false},{"name":"9","selected":false},{"name":"10","selected":false},{"name":"11","selected":false},{"name":"12","selected":false},{"name":"13","selected":false},{"name":"14","selected":false},{"name":"15","selected":false},{"name":"16","selected":false},{"name":"17","selected":false},{"name":"18","selected":false},{"name":"19","selected":false},{"name":"20","selected":false}];
    selectedProduct : any;
    childData : any;
    constructor(public translate: TranslateService, public events: Events, public loading: LoadingController, public productService: ProductServiceProvider, public navCtrl: NavController, public navParams: NavParams,public viewCtrl:ViewController,private picker:PickerController) {
         // this.createPicker();
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad HotmealsnoPage');
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }

    ionViewWillEnter() {
        this.selectedProduct = this.navParams.data.product;
        this.childData = this.navParams.data.childData;
        var isEditItem = JSON.parse(localStorage.getItem('editItem'));

        if(isEditItem != null && isEditItem != undefined) {
            this.items.forEach(function (element) {
                if(element.name == isEditItem.quantity) {
                    element.selected = true;
                }
            })
        }
    }

    onQuantityClick(item) {
        this.items.forEach(function (element) {
            element.selected = false;
        }.bind(this));

        item.selected = true;
    }

    onTapSaveButton() {
        let anyQuantity = '';
        this.items.forEach(function (element) {
            if(element.selected) {
                anyQuantity = element.name;
            }
        }.bind(this));


        if(anyQuantity !== '') {
            var regDatId = localStorage.getItem('regDayId');
            var regId = localStorage.getItem('regId');

            var isEditItem = JSON.parse(localStorage.getItem('editItem'));
            var key = 'ADDING PRODUCT';
            if(isEditItem != null) {
                key = 'UPDATING PRODUCT';
            }

            let loader = this.loading.create({
                content: this.translate.instant(key)
            });
            loader.present();

            let productObj = {date: moment().format('YYYY-MM-DD'), product_id : this.selectedProduct.id, quantity :anyQuantity, registration_day_id :regDatId, registration_id : regId}
            let userDetails = JSON.parse(localStorage.getItem('userDetail'));

            this.productService.saveUpdateProduct(userDetails , productObj).then((data) => {
                loader.dismissAll();
                this.closePopupsAndShowProduct();
            },(data) => {
                loader.dismissAll();
                let selectedItem = {type : this.selectedProduct.type, name:this.selectedProduct.name, multiple_possible : this.selectedProduct.multiple_possible, id: this.selectedProduct.id, quantity : anyQuantity};
                this.productService.setOfflineProducts(this.childData.child_id, selectedItem);
                this.closePopupsAndShowProduct();
            });
        }
    }

    closePopupsAndShowProduct () {
        let firstViewCtrl = this.navCtrl.first();
        this.navCtrl.popToRoot({animate: false}).then(() =>  {
            firstViewCtrl.dismiss();
            this.events.publish('productAdded');
        });
    }

    createPicker():void {
        let picker = this.picker.create({
            buttons: [{ text: 'accept' }]
        });

        let column = { columnWidth: '23%',options: this.items };

        picker.addColumn(column);
        picker.present(picker);
    }
}
    
