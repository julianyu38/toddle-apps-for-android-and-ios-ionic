import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HotmealsnoPage } from './hotmealsno';

@NgModule({
  declarations: [
    HotmealsnoPage,
  ],
  imports: [
    IonicPageModule.forChild(HotmealsnoPage),
  ],
  exports: [
    HotmealsnoPage
  ]
})
export class HotmealsnoPageModule {}
