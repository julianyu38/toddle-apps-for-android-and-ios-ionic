import { Component } from '@angular/core';
import {
    AlertController, Events, IonicPage, LoadingController, NavController, NavParams,
    ViewController
} from 'ionic-angular';
import {PickerController} from 'ionic-angular';
import {ProductServiceProvider} from "../../providers/product-service/product-service";
import * as moment from 'moment/moment';
import {TranslateService} from '@ngx-translate/core';
import {Network} from "@ionic-native/network";
import {KidsListServiceProvider} from "../../providers/kids-list-service/kids-list-service";
import {Globals} from "../../app/globals";
@IonicPage()
@Component({
  selector: 'page-edithours',
  templateUrl: 'editHours.html',
   
})

export class EditHours {

    times : any;
    userDetails: any;
    childData : any;
    childInOuts: any;
    cDate : any;
    isMultiple : Boolean;
    constructor(public alertCtrl: AlertController, public kidList: KidsListServiceProvider,public network: Network, public translate: TranslateService, public events: Events, public loading: LoadingController, public productService: ProductServiceProvider, public navCtrl: NavController, public navParams: NavParams,public viewCtrl:ViewController,private picker:PickerController) {
        this.times = [{in:'', out:''},{in:'', out:''},{in:'', out:''},{in:'', out:''}];
        this.userDetails = JSON.parse(localStorage.getItem('userDetail'));
        this.childData = this.navParams.data.childData;
        this.childInOuts = this.productService.getChildInOuts(this.userDetails.data.user_id, this.childData.child_id);
        console.log(this.childInOuts);
        this.createEditTimeArray();
        this.isMultiple = Globals.IS_MULTIPLE_HOURS;
    }

    private createEditTimeArray() {
        var tempArray = []
        this.childInOuts.forEach(function (element,  index) {
            console.log(element);
            this.cDate = new Date();
            var hm = element.split(":");
            this.cDate.setHours(hm[0]);
            this.cDate.setMinutes(hm[1]);

            let tzoffset = (new Date()).getTimezoneOffset() * 60000; //offset in milliseconds
            let localISOTime = (new Date(this.cDate - tzoffset)).toISOString().slice(0,-1);

            console.log(localISOTime);
            tempArray.push(localISOTime);
        }.bind(this));

        var count = 0;
        tempArray.forEach(function (element,  index) {
            if(index % 2 == 0) {
                this.times[count].in = tempArray[index];
                if(tempArray[index + 1] != undefined) this.times[count].out = tempArray[index + 1];
                count++;
            }
        }.bind(this));

        /*let tzoffset = (new Date()).getTimezoneOffset() * 60000; //offset in milliseconds
        let localISOTime = (new Date(Date.now() - tzoffset)).toISOString().slice(0,-1);*/
        console.log(this.times);
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad EditHours');
    }

    ionViewWillEnter() {

    }

    dismiss() {
        this.viewCtrl.dismiss();
    }

    removeInHours(model) {
        // if (this.times[0].out == "" || this.times[0].out == null)
           this.times[model].in = "";
        // else {
        //     let alert = this.alertCtrl.create({
        //         title: 'Alert',
        //         subTitle: "You Can't Remove Check in Time",
        //         buttons: ['Ok']
        //     });
        //     alert.present();
        // }
    }
    
    removeHours(index?){
        
        if(index != undefined){
            this.times[index].out = "";
            this.times[index].in = "";
        }        
        else{
            this.times[0].in = "";
            this.times[0].out = "";
        }
       
    }

    removeOutHours(model) {
        this.times[model].out = "";
    }

    getISOTime(hhmm) {
        this.cDate = new Date();
        var hm = hhmm.split(":");
        this.cDate.setHours(hm[0]);
        this.cDate.setMinutes(hm[1]);

        let tzoffset = (new Date()).getTimezoneOffset() * 60000; //offset in milliseconds
        return (new Date(this.cDate - tzoffset)).toISOString().slice(0,-1);
    }

    updateLocalTime(valuesArray) {
        var cUserData = JSON.parse(localStorage.getItem('childData'));
        var isNewReg = true;
        for(var i=0; i< cUserData.length; i++) {
            if(cUserData[i].cId == this.childData.child_id) {
                cUserData[i].values = valuesArray;
                isNewReg = false;
                break;
            }
        }

        if(isNewReg) {
            cUserData.push({"cId":this.childData.child_id,"values":valuesArray});
        }

        localStorage.setItem('childData', JSON.stringify(cUserData));
        this.events.publish('timeUpdate');
    }
    onTapSaveButton() {
        var _self = this;

        var childObj =      {
            "user_id": this.userDetails.data.user_id,
            "attendance_detail":{
                "child": {
                    "date": moment().format('YYYY-MM-DD'),
                    "child_id": this.childData.child_id,
                    "location_id": Number(localStorage.getItem('currentLocationId')),
                    "registration_id": this.childData.registration_id,
                    "registration_day_id": null,
                    "hour_in1": '',
                    "hour_in2": '',
                    "hour_in3": '',
                    "hour_in4": '',
                    "hour_out1": '',
                    "hour_out2": '',
                    "hour_out3": '',
                    "hour_out4": '',
                    "remarks" : this.childData.child_info
                }
            }
        };

        // While edit blank hours first time.
        this.times.forEach(function (element) {
            if(element.in.length > 0 && element.in.length < 6) {
                element.in = this.getISOTime(element.in);
            }

            if(element.out.length > 0 && element.out.length < 6) {
                element.out = this.getISOTime(element.out);
            }
        }.bind(this));

        console.log(this.times);

        var errorMsg = '';
        var valuesArray = []
        if(this.times[0].in != '') {
            childObj.attendance_detail.child.hour_in1 = moment.utc(this.times[0].in).format("HH:mm");
            valuesArray.push(childObj.attendance_detail.child.hour_in1);
        }

        if(this.times[0].out != '') {
            if(this.times[0].in == '' || moment.utc(this.times[0].in) > moment.utc(this.times[0].out)) {
                errorMsg = 'CHECK IN TIME SHOULD BE LESS FROM CHECK OUT TIME!';
            }
            childObj.attendance_detail.child.hour_out1 = moment.utc(this.times[0].out).format("HH:mm");
            valuesArray.push(childObj.attendance_detail.child.hour_out1);
        }

        if(this.times[1].in != '') {
            childObj.attendance_detail.child.hour_in2 = moment.utc(this.times[1].in).format("HH:mm");
            valuesArray.push(childObj.attendance_detail.child.hour_in2);
        }

        if(this.times[1].out != '') {
            if(this.times[1].in == '' || moment.utc(this.times[1].in) > moment.utc(this.times[1].out) || moment.utc(this.times[1].in) < moment.utc(this.times[0].out)) {
                errorMsg = 'CHECK IN TIME SHOULD BE LESS FROM CHECK OUT TIME!';
            }
            childObj.attendance_detail.child.hour_out2 = moment.utc(this.times[1].out).format("HH:mm");
            valuesArray.push(childObj.attendance_detail.child.hour_out2);
        }

        if(this.times[2].in != '') {
            childObj.attendance_detail.child.hour_in3 = moment.utc(this.times[2].in).format("HH:mm");
            valuesArray.push(childObj.attendance_detail.child.hour_in3);
        }

        if(this.times[2].out != '') {
            if(this.times[2].in == '' || moment.utc(this.times[2].in) > moment.utc(this.times[2].out) || moment.utc(this.times[2].in) < moment.utc(this.times[1].out) || moment.utc(this.times[2].in) < moment.utc(this.times[0].out)) {
                errorMsg = 'CHECK IN TIME SHOULD BE LESS FROM CHECK OUT TIME!';
            }

            childObj.attendance_detail.child.hour_out3 = moment.utc(this.times[2].out).format("HH:mm");
            valuesArray.push(childObj.attendance_detail.child.hour_out3);
        }

        if(this.times[3].in != '') {
            childObj.attendance_detail.child.hour_in4 = moment.utc(this.times[3].in).format("HH:mm");
            valuesArray.push(childObj.attendance_detail.child.hour_in4);
        }

        if(this.times[3].out != '') {
            if(this.times[3].in == '' || moment.utc(this.times[3].in) > moment.utc(this.times[3].out) || moment.utc(this.times[3].in) < moment.utc(this.times[2].out) || moment.utc(this.times[3].in) < moment.utc(this.times[1].out) || moment.utc(this.times[3].in) < moment.utc(this.times[0].out)) {
                errorMsg = 'CHECK IN TIME SHOULD BE LESS FROM CHECK OUT TIME!';
            }

            childObj.attendance_detail.child.hour_out4 = moment.utc(this.times[3].out).format("HH:mm");
            valuesArray.push(childObj.attendance_detail.child.hour_out4);
        }


        if(errorMsg == '') {
            let loader = this.loading.create({
                content: this.translate.instant('UPDATING')
            });
            loader.present();

            this.kidList.editRegistrationOfChild(this.userDetails, childObj).then((data) => {
                _self.dismiss();
                loader.dismissAll();
                _self.updateLocalTime(valuesArray);
            },(data) => {
                _self.dismiss();
                _self.updateLocalTime(valuesArray);
                loader.dismissAll();
            });

        } else {
            let alert = this.alertCtrl.create({
                title: this.translate.instant('Error'),
                subTitle: this.translate.instant(errorMsg),
                buttons: ['Ok']
            });
            alert.present();
        }
    }
}
    
