import {Component, OnInit} from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {FiltersServiceProvider} from "../../providers/filters-service/filters-service";

@IonicPage()
@Component({
  selector: 'page-popup-model',
  templateUrl: 'popup-model.html'
})
export class PopupModelPage implements OnInit{
  child_groups:any;
    LOCATION_SELECTOR_POPUP_TITLE:any;
  ngOnInit(): void {
    this.child_groups = JSON.parse(JSON.stringify(this.navParams.data));
    this.LOCATION_SELECTOR_POPUP_TITLE = this.filterservice.getTitleForPopUp();
  }
  constructor(public filterservice:FiltersServiceProvider, public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {

  }

    dismiss() {
        this.viewCtrl.dismiss();
    }

    onSavePopup() {
        this.filterservice.setPopupData(this.child_groups);
        this.viewCtrl.dismiss();
    }

    onSelectItem(item) {
      console.log("onSelectItem");
       if(item.child_group_name == 'All' && !item.selected) {
           this.child_groups.forEach(function (element) {
               element.selected = true;
           })
       } else if(item.child_group_name == 'All' && item.selected) {
               this.child_groups.forEach(function (element) {
                   element.selected = false;
               })
       } else {
           item.selected = !item.selected;
           this.child_groups.forEach(function (element) {
               if(element.child_group_name == 'All' && item.child_group_name != 'All') {
                   element.selected = false;
               }
           })
       }
    }
}
