import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import {Headers, Http} from '@angular/http';
import * as moment from 'moment/moment';
import {Globals} from "../../app/globals";
import {CommandHandler} from "../commandhandler";

@Injectable()
export class ProductServiceProvider {
    url = 'https://mobile-kdv1.tactics.be/app.php/';
    serverProducts: any;
    constructor(public http: Http, public commandHandler : CommandHandler) {}

    getSavedProducts() {
        return this.serverProducts;
    }

    setSavedProducts(data) {
        this.serverProducts = data;
    }

    getAllProducts(userDetailObj, productObj) {
        return new Promise((resolve, reject) => {
            var header = new Headers();
            header.append("city_key", userDetailObj.installation_key);
            header.append("token", userDetailObj.data.token);
            header.append('Content-Type', 'application/json');
            this.http.get(this.url +productObj.locationId+'/'+productObj.date+'/'+productObj.userId+'/products', {headers: header})
                .map(res => res.json())
                .subscribe(data =>{
                        resolve(data);
                    },
                    error => {
                        reject(error);
                        this.commandHandler.showAPIError(error);
                    },
                    () => console.log("Finished")
                );
        });

    }

    getCurrentChildProducts(userDetailObj, productStr) {
        return new Promise((resolve, reject) => {
            var header = new Headers();
            header.append("city_key", userDetailObj.installation_key);
            header.append("token", userDetailObj.data.token);
            header.append('Content-Type', 'application/json');
            this.http.get(this.url +"child/"+ productStr +'/products', {headers: header})
                .map(res => res.json())
                .subscribe(data =>{
                        resolve(data);
                    },
                    error => {
                        reject(error);
                        this.commandHandler.showAPIError(error);
                    },
                    () => console.log("Finished")
                );
        });

    }

    setOfflineProducts(cId, product) {
        var tempProd = JSON.parse(localStorage.getItem('newlyAddedProduct'));
        if(tempProd === null) {
            localStorage.setItem('newlyAddedProduct', JSON.stringify([]));
            tempProd = [];
        }

        product.selected = false;
        product.child_id = cId;

        let isSameProd = false;
        tempProd.forEach(function (element) {
             if(product.id == element.id) {
                 isSameProd = true;
                 element.quantity = product.quantity;
             }
        });

        if(!isSameProd) tempProd.push(product);
        localStorage.setItem('newlyAddedProduct', JSON.stringify(tempProd));
    }

    saveUpdateProduct(userDetailObj, productObj) {
        return new Promise((resolve, reject) => {
            var header = new Headers();
            header.append("city_key", userDetailObj.installation_key);
            header.append("token", userDetailObj.data.token);
            header.append('Content-Type', 'application/json');
            var _url = this.url + userDetailObj.data.user_id+'/save/product';
            this.http.post(_url, productObj, {headers: header})
                .map(res => res.json())
                .subscribe(data =>{
                        resolve(data);
                    },
                    error => {
                        reject(error);
                        if(error.status === 0) {
                            this.commandHandler.updateToSyncQueue(_url, 'post', productObj);
                        } else this.commandHandler.showAPIError(error);

                    },
                    () => console.log("Finished")
                );
        });

    }

    deleteProductIntoQueue(userId, delObj) {
        var _url = this.url + userId +'/delete/product';
        this.commandHandler.updateToSyncQueue(_url, 'delete', delObj);
    }

    deleteProduct(userDetailObj, delObj) {
        console.log(delObj);
        return new Promise((resolve, reject) => {
            console.log(delObj)
            console.log(this.url + userDetailObj.data.user_id+'/delete/product')
            var header = new Headers();
            header.append("city_key", userDetailObj.installation_key);
            header.append("token", userDetailObj.data.token);
            header.append('Content-Type', 'application/json');
            console.log(header)
            this.http.post(this.url + userDetailObj.data.user_id+'/delete/product',delObj,{headers: header})
                .map(res => res.json())
                .subscribe(data =>{
                        resolve(data);
                    },
                    error => {
                        reject(error);
                        this.commandHandler.showAPIError(error);
                    },
                    () => console.log("Finished")
                );
        });

    }

    saveChildComments(userDetailObj, commentObj) {
        return new Promise((resolve, reject) => {
            var header = new Headers();
            header.append("city_key", userDetailObj.installation_key);
            header.append("token", userDetailObj.data.token);
            header.append('Content-Type', 'application/json');
            var _url = this.url + userDetailObj.data.user_id+'/save/comment';
            this.http.post(_url, commentObj,{headers: header})
                .map(res => res.json())
                .subscribe(data =>{
                        resolve(data);
                    },
                    error => {
                        reject(error);
                        if(error.status === 0) {
                            this.commandHandler.updateToSyncQueue(_url, 'post', commentObj);
                        } else this.commandHandler.showAPIError(error);
                    },
                    () => console.log("Finished")
                );
        });

    }

    photoUploadForChild(userDetailObj, photoObj) {
        return new Promise((resolve, reject) => {
            var header = new Headers();
            header.append("city_key", userDetailObj.installation_key);
            header.append("token", userDetailObj.data.token);
            header.append('Content-Type', 'application/json');
            var _url = this.url + userDetailObj.data.user_id +'/child/save/photo';
            this.http.post(_url, photoObj, {headers: header})
                .map(res => res.json())
                .subscribe(data =>{
                        resolve(data);
                    },
                    error => {
                        reject(error);
                        if(error.status === 0) {
                            this.commandHandler.updateToSyncQueue(_url, 'post', photoObj);
                        } else this.commandHandler.showAPIError(error);
                    },
                    () => console.log("Finished")
                );
        });

    }

    getPhotoFromUser(url) {
        var userDetailObj = JSON.parse(localStorage.getItem('userDetail'));
        return new Promise((resolve, reject) => {
            var header = new Headers();
            header.append("city_key", userDetailObj.installation_key);
            header.append("token", userDetailObj.data.token);
            header.append('Content-Type', 'application/json');
            this.http.get(url, {headers: header})
                .map(res => res.json())
                .subscribe(data =>{
                        resolve(data);
                    },
                    error => {
                        reject(error);
                        this.commandHandler.showAPIError(error);
                    },
                    () => console.log("Finished")
                );
        });

    }

    getChildInOuts  = (userId, childId) => {
        var localChildData = JSON.parse(localStorage.getItem('childData'));
        if (localChildData != null) {
            var childIndex = null;

            localChildData.forEach(function (element, index) {
                if (element.cId == childId) {
                    childIndex = index;
                }
            });

            if(childIndex != null) {
                return localChildData[childIndex].values;
            } else {
                return []
            }

        } else {
            return [];
        }
    }
}
