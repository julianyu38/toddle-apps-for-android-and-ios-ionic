export class Globals {
  public static IS_MULTIPLE_HOURS = false;
  public static ATTEMPTS = 2;
  public static IS_NETWORK_UP = true;
  public static SYNC_STARTED = false;
  public static LOCATION_NAME = 'HOME';
  public static CHILD_IMAGE_URLs = [];
}
