import {Component, OnInit} from '@angular/core';
import {Events, ModalController, NavParams, ViewController} from 'ionic-angular';
import {FiltersServiceProvider} from "../../providers/filters-service/filters-service";
import {PopupModelPage} from "../popup-model/popup-model";
@Component({
  selector: 'page-popover',
  templateUrl: 'popover.html',
})
export class PopoverContentPage implements OnInit{

    structure: any
    child_groups_array :any;
    planned_array :any;
    schedule_array :any;
    mColor:boolean = true;
    vColor:boolean = true;
    constructor(public viewCtrl: ViewController,
              public filterservice:FiltersServiceProvider,
              public modalCtrl:ModalController,
              public params: NavParams,
              public events: Events) {
    }

  selectGender(gender:string) {
    if(gender=="M") {
        this.mColor = !this.mColor;
        if(!this.mColor) {
            this.vColor = true;
        }
    }else{
        this.vColor = !this.vColor;
        if(!this.vColor) {
            this.mColor = true;
        }
    }
  }
    resetFilter () {
        localStorage.removeItem('age');
        localStorage.removeItem('male');
        localStorage.removeItem('female');
        localStorage.removeItem('savedGroups');
        localStorage.removeItem('savedPlanned');
        localStorage.removeItem('savedSchedule');

        this.filterservice.resetAllPopupData(undefined);

        if(this.child_groups_array != undefined) {
            this.child_groups_array.forEach(function (ele) {
                ele.selected = true;
            })
        }

        if(this.planned_array != undefined) {
            this.planned_array.forEach(function (ele) {
                ele.selected = true;
            })
        }

        if(this.schedule_array != undefined) {
            this.schedule_array.forEach(function (ele) {
                ele.selected = true;
            })
        }

        this.structure = {lower: 0, upper: 6};
        this.mColor = true;
        this.vColor = true;

        this.events.publish('filterApply');
        this.close();
    }

    applyFilter () {
      this.filterservice.setFilter(this.structure, this.mColor, this.vColor);
      if(this.child_groups_array != undefined) this.filterservice.storeDataInLocal('savedGroups', this.filterservice.getPopupData('group'));
      if(this.planned_array != undefined) this.filterservice.storeDataInLocal('savedPlanned', this.filterservice.getPopupData('planned'));
      if(this.schedule_array != undefined) this.filterservice.storeDataInLocal('savedSchedule', this.filterservice.getPopupData('schedule'));

      this.events.publish('filterApply');
      this.close();
  }

  ngOnInit(): void {
      if(localStorage.getItem('age') === null) {
          this.structure = {lower: 0, upper: 6};
      } else {
          this.structure = JSON.parse(localStorage.getItem('age'));
      }

      if(localStorage.getItem('male') === null) {
          this.mColor = true;
      } else {
          this.mColor = localStorage.getItem('male') === 'true' ? true : false;
      }

      if(localStorage.getItem('female') === null) {
          this.vColor = true;
      } else {
          this.vColor = localStorage.getItem('female') === 'true' ? true : false;
      }

    this.child_groups_array = this.params.data.child_group;

  }

    close()
    {
        this.viewCtrl.dismiss();
    }

    chooseGroup()
    {
        this.filterservice.setCurPopup('group');
        if(localStorage.getItem('savedGroups') == null || localStorage.getItem('savedGroups') == 'undefined') {
            if(this.filterservice.getPopupData('group') != undefined) {
                this.child_groups_array = this.filterservice.getPopupData('group');
            } else {
                var addAll = true;
                this.child_groups_array.forEach(function (element) {
                    if(element.child_group_name == 'All') {
                        addAll = false;
                    }
                });

                if(addAll) {
                    this.child_groups_array.unshift({child_group_name : 'All', selected: true});
                    this.child_groups_array.forEach(function (element) {
                        element.selected = true;
                    });
                }
            }
        } else {
            if(this.filterservice.getPopupData('group') != undefined) {
                this.child_groups_array = this.filterservice.getPopupData('group');
            } else {
                this.child_groups_array = JSON.parse(localStorage.getItem('savedGroups'));
            }
        }

        let profileModal = this.modalCtrl.create(PopupModelPage, this.child_groups_array);
        profileModal.present();
    }

    choosePlanned()
    {
        this.filterservice.setCurPopup('planned');

        if(localStorage.getItem('savedPlanned') == null || localStorage.getItem('savedPlanned') == 'undefined') {
            if(this.filterservice.getPopupData('planned') != undefined) {
                this.planned_array = this.filterservice.getPopupData('planned');
            } else {
                this.planned_array = [{child_group_name : 'All', selected : true}, {child_group_name : 'Planned', selected: true}, {child_group_name : 'Unplanned', selected: true}];
            }
        } else {
            if(this.filterservice.getPopupData('planned') != undefined) {
                this.planned_array = this.filterservice.getPopupData('planned');
            } else {
                this.planned_array = JSON.parse(localStorage.getItem('savedPlanned'));
            }
        }


        console.log(this.planned_array);

        let profileModal = this.modalCtrl.create(PopupModelPage, this.planned_array);
        profileModal.present();
    }

    chooseSchedule()
    {
        this.filterservice.setCurPopup('schedule');

        if(localStorage.getItem('savedSchedule') == null || localStorage.getItem('savedSchedule') == 'undefined') {
            if(this.filterservice.getPopupData('schedule') != undefined) {
                this.schedule_array = this.filterservice.getPopupData('schedule');
            } else {
                this.schedule_array = [{child_group_name : 'All', selected: true}, {child_group_name : 'Checked In', selected: true}, {child_group_name : 'Checked Out', selected: true}, {child_group_name : 'Absent', selected: true}];
            }
        } else {
            if(this.filterservice.getPopupData('schedule') != undefined) {
                this.schedule_array = this.filterservice.getPopupData('schedule');
            } else {
                this.schedule_array = JSON.parse(localStorage.getItem('savedSchedule'));
            }
        }

        let profileModal = this.modalCtrl.create(PopupModelPage, this.schedule_array);
        profileModal.present();
    }

}
