import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {EditHours} from "./editHours";

@NgModule({
  declarations: [
      EditHours,
  ],
  imports: [
    IonicPageModule.forChild(EditHours),
  ],
  exports: [
      EditHours
  ]
})
export class EditHoursPageModule {}
