import { Component, ViewChild } from '@angular/core';
import {Events, Platform} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPage } from "../pages/login/login";
import { KidslistPage } from "../pages/kidslist/kidslist";
import { TranslateService } from '@ngx-translate/core';
import { Sqlite } from "../providers/kids-database/sqlite";
import { Device } from '@ionic-native/device';

@Component({
  templateUrl: 'app.html'
})
//export class MyApp {
export class MyApp {
  @ViewChild('nav') nav;
  rootPage: any;
  userdata: any;
  userDetails: { [k: string]: any } = {};

  constructor(public events: Events, public kidsDb: Sqlite, public device: Device, public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, private translate: TranslateService) {
    this.translate.setDefaultLang('du');
    this.platform.ready().then(() => {
        this.initializeApp();
        this.statusBar.styleDefault();
        this.splashScreen.hide();
    });


      this.events.unsubscribe('goToLogin');

      events.subscribe('goToLogin', () => {
           this.nav.setRoot(LoginPage);
      });
  }

  initializeApp() {
    localStorage.removeItem('savedProducts');
    localStorage.removeItem('savedGroups');
    localStorage.removeItem('savedPlanned');
    localStorage.removeItem('savedSchedule');
    localStorage.removeItem('age');
    localStorage.removeItem('male');
    localStorage.removeItem('female');

    if(this.device.platform === 'iOS' || this.device.platform === 'Android'  || this.device.platform === 'android') {
        localStorage.setItem('isDevice', 'true');
        this.kidsDb.openDb();

    } else {
        localStorage.setItem('isDevice', 'false');
    }

      if(localStorage.getItem('userDetail') == null) {
          this.rootPage = LoginPage;
      } else {
          this.nav.setRoot(KidslistPage, JSON.parse(localStorage.getItem('userDetail')));
      }

  }
}
