import { Component, OnInit } from '@angular/core';
import { AlertController, NavController, Platform } from 'ionic-angular';
import { KidslistPage } from '../kidslist/kidslist';
import { LoginServiceProvider } from "../../providers/login-service/login-service";
import { LoadingController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { CommandHandler } from "../../providers/commandhandler";

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage implements OnInit {

  KidslistPage1 = KidslistPage;
  username: any;
  password: any;
  installation_key: any;
  userDetails: { [k: string]: any } = {};


  constructor(public navCtrl: NavController,
    public loginReq: LoginServiceProvider,
    public loading: LoadingController,
    public alertCtrl: AlertController,
    public translate: TranslateService,
    public commandHandler: CommandHandler,
    public platform: Platform) {

    // this.username = 'api.test@tactics.be';
    // this.password = 'passw';
    // this.installation_key = 'demo_acc';

    this.username='';
    this.password='';
    this.installation_key ='';
  }

  ngOnInit() {

  }

  showErrorMsg() {
    let alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: 'Please provide valid login information!',
      buttons: ['Ok']
    });
    alert.present();
  }

  login() {
    let self = this;
    if (this.username != '' && this.password != '') {
      self.commandHandler.showLoader('Login...');

      this.loginReq.login(this.username, this.password, this.installation_key).then((data) => {
        this.userDetails.data = data;   //----=== getting id & token from api after storing data in userInfo table 1st time
        this.userDetails.installation_key = this.installation_key;
        this.userDetails.password = this.password;
        localStorage.setItem('userDetail', JSON.stringify(this.userDetails));
        this.navCtrl.setRoot(KidslistPage, this.userDetails);  //---------====== passing id,token,pass,key to kidlist.ts
        self.commandHandler.hideLoader();
      }, function (error) {
        console.log(error);
        if (error.status == 0) {
          self.commandHandler.showAPIError(error);
        } else {
          self.commandHandler.hideLoader();
          self.showErrorMsg();
        }
      });
    } else {
      self.showErrorMsg();
    }
  }
}
