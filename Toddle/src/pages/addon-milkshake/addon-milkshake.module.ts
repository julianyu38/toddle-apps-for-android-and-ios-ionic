import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddonMilkshakePage } from './addon-milkshake';

@NgModule({
  declarations: [
    AddonMilkshakePage,
  ],
  imports: [
    IonicPageModule.forChild(AddonMilkshakePage),
  ],
  exports: [
    AddonMilkshakePage
  ]
})
export class AddonMilkshakePageModule {}
