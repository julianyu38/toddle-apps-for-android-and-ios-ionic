import { Injectable } from '@angular/core';
//import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import {Headers, Http} from '@angular/http';
import * as moment from 'moment/moment';
import {reject} from "q";
import {AlertController, Events, LoadingController} from "ionic-angular";
import {TranslateService} from "@ngx-translate/core";
import {FiltersServiceProvider} from "./filters-service/filters-service";


@Injectable()
export class CommandHandler {

    loader : any;
    constructor(public http: Http, public events: Events, public filterData: FiltersServiceProvider, public translate: TranslateService,public alertCtrl: AlertController, public loading: LoadingController) {

    }

    syncLocalToServer(apiObj, index) {
        console.log(apiObj);
        return new Promise((resolve, reject) => {
            let userDetails = JSON.parse(localStorage.getItem('userDetail'));
            var header = new Headers();
            header.append("city_key", userDetails.installation_key);
            header.append("token", userDetails.data.token);
            header.append('Content-Type', 'application/json');
            var promiseObj;
            if(apiObj.type === 'post') {
                promiseObj = this.http.post(apiObj.url, apiObj.payload, {headers: header});
            } else if(apiObj.type === 'delete') {
                promiseObj = this.http.delete(apiObj.url, {headers: header, body: apiObj.payload});
            } else if(apiObj.type === 'put') {
                promiseObj = this.http.put(apiObj.url, apiObj.payload, {headers: header});
            }

            promiseObj.map(res => res.json())
                .subscribe(data =>{
                        resolve(index);
                    },
                    error => {
                        reject(index);
                    },
                    () => console.log("Finished")
                );
        });
    }

    updateToSyncQueue(_url, _type, _payload) {

        var tempQueItems = (JSON.parse(localStorage.getItem("syncQueue")));
        if(tempQueItems == null) tempQueItems = [];
        tempQueItems.push({url : _url, type : _type, payload: _payload, isQueued: false});

        localStorage.setItem("syncQueue", JSON.stringify(tempQueItems));

        if(tempQueItems.length == 1) {
            localStorage.setItem('offlineStartTime', String(new Date().getTime()));
        }
    }

    allSyncDone() {
        localStorage.removeItem('syncQueue');
        localStorage.removeItem('offlineStartTime');
        localStorage.removeItem('newlyAddedProduct');
        this.hideLoader();

        this.events.publish('syncCompleted');
    }

    checkSyncToServer() {
        console.log("checkSyncToServer");
        var self = this;
        var syncData = JSON.parse(localStorage.getItem('syncQueue'));
        if(syncData != null && syncData.length > 0) {
            console.log("syncData");
            console.log(syncData);
            for(var i=0; i < syncData.length; i++)
            {
                if(syncData[i] != undefined)
                {
                    this.syncLocalToServer(syncData[i], i).then(function(resp){
                        console.log("Success syncSQueue APP resp : "+ resp);
                        //syncData[resp.index].isQueued = true;
                        if((syncData.length - 1) == resp) {
                            self.allSyncDone();
                        }
                    },function(resp){
                        console.log("Error syncSQueue APP resp : "+resp);
                        if((syncData.length - 1) == resp) {
                            self.allSyncDone();
                        }
                    });
                }
            }
        }
    }

    showLoader (_key) {
        this.loader = this.loading.create({
            content: this.translate.instant(_key)
        });
        this.loader.present();
    }

    hideLoader () {
        this.loader.dismissAll();
    }

    showAPIError (errorObj, isPopup= true) {
        console.log("errorObj errorObj");
        console.log(errorObj);
        this.hideLoader();

        var title, content, btnText;
        btnText = this.translate.instant('Ok');
        if(errorObj.status == 401 || errorObj.statusText == 'Unauthorized') {
            title = this.translate.instant('Error');
            content = this.translate.instant('Session Expired!');
        } else if(errorObj.status == 0) {
            title = 'Error';
            content = 'No Internet Available!';
        } else {
            title = 'Error';
            content = 'Internal Server Error!';
        }

        if(isPopup) {
            let alert = this.alertCtrl.create({
                title:this.translate.instant(title),
                subTitle: this.translate.instant(content),
                buttons: [btnText]
            });

            alert.present();

            if(content == 'Session Expired!') {
                this.goBackToLogin();
            }
        }
    }

    showAlert (title, content) {
        let alert = this.alertCtrl.create({
            title:this.translate.instant(title),
            subTitle: this.translate.instant(content),
            buttons: [this.translate.instant('Ok')]
        });

        alert.present();
    }

    goBackToLogin() {
        this.resetCommandStorage();
        localStorage.removeItem('userDetail');
        localStorage.removeItem('savedProducts');
        this.events.publish('goToLogin');
    }

    resetCommandStorage() {
        localStorage.removeItem('savedGroups');
        localStorage.removeItem('savedPlanned');
        localStorage.removeItem('savedSchedule');
        localStorage.removeItem('age');
        localStorage.removeItem('male');
        localStorage.removeItem('female');

        this.filterData.resetAllPopupData(undefined);
    }
}
