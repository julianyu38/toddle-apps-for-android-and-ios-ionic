import { Component } from '@angular/core';
import { Events, NavController, NavParams, LoadingController, AlertController, ModalController } from 'ionic-angular';
import { NotesPage } from '../../pages/notes/notes';
import * as moment from 'moment/moment';
import { KidsListServiceProvider } from "../../providers/kids-list-service/kids-list-service";
import { Network } from '@ionic-native/network';
import { Globals } from "../../app/globals";
import { ProductServiceProvider } from "../../providers/product-service/product-service";
import { Camera, CameraOptions } from '@ionic-native/camera';
import { TranslateService } from '@ngx-translate/core';
import { EditHours } from "../edithours/editHours";
import { CommandHandler } from "../../providers/commandhandler";
import { Sqlite } from "../../providers/kids-database/sqlite";

/**
 * Generated class for the ProfilePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
    selector: 'page-profile',
    templateUrl: 'profile.html',
})
export class ProfilePage {
    name: any;
    defaultTap: string;
    userDetails: any;

    constructor(public kidsDb: Sqlite, public commandHandler: CommandHandler, public events: Events, public modalCtrl: ModalController, public translate: TranslateService, public camera: Camera, public alertCtrl: AlertController, public loading: LoadingController, public navCtrl: NavController, public navParams: NavParams, public kidList: KidsListServiceProvider, public productService: ProductServiceProvider, public network: Network) {
        this.name = this.navParams.data;
        console.log(this.name);
        localStorage.setItem('currentChild', JSON.stringify({ id: this.name.child_id, value: this.name.selected, visited: this.name.visited }));
        events.subscribe('timeUpdate', () => {
            this.name.child_time = this.kidList.getChildHours(this.userDetails.data.user_id, this.name.child_id);
            this.name.selected = this.name.child_time.selected;
            this.name.visited = this.name.child_time.visited;
        });
    }

    ionViewWillEnter() {
        this.defaultTap = 'con';
        this.userDetails = JSON.parse(localStorage.getItem('userDetail'));
        this.name.child_time = this.kidList.getChildHours(this.userDetails.data.user_id, this.name.child_id);
        this.name.selected = this.name.child_time.selected;
        this.name.visited = this.name.child_time.visited;
    }

    ionViewDidLeave() {
        this.events.unsubscribe('timeUpdate');
    }

    onTapProfileImage = () => {
        let options: CameraOptions = {
            quality: 80,
            destinationType: this.camera.DestinationType.DATA_URL,
            mediaType: this.camera.MediaType.PICTURE,
            encodingType: this.camera.EncodingType.JPEG,
            saveToPhotoAlbum: false,
            correctOrientation: true,
            sourceType: this.camera.PictureSourceType.CAMERA,
            allowEdit: true,
            targetWidth: 300,
            targetHeight: 300
        };

        this.camera.getPicture(options).then((imageData) => {

            this.name.child_image_basedata = 'data:image/jpeg;base64,' + imageData;

            if (Globals.CHILD_IMAGE_URLs.length > 0) {
                var isMatched = false;
                for (var i = 0; i < Globals.CHILD_IMAGE_URLs.length; i++) {
                    if (Globals.CHILD_IMAGE_URLs[i].child_id == this.name.child_id) {
                        isMatched = true;
                        Globals.CHILD_IMAGE_URLs[i].data = this.name.child_image_basedata;
                        break;
                    }
                }

                if (!isMatched) {
                    Globals.CHILD_IMAGE_URLs.push({ child_id: this.name.child_id, data: this.name.child_image_basedata });
                }
            }

            this.commandHandler.showLoader('SAVING IMAGE');

            var photoObj = { child_id: this.name.child_id, bytes: imageData };
            this.productService.photoUploadForChild(this.userDetails, photoObj).then((data) => {
                console.log(data);
                this.commandHandler.hideLoader();
            }, (data) => {
                this.commandHandler.hideLoader();
            });

        }, (err) => {
            alert(err);
        });

    }

    onClickTabs = (type) => {
        this.defaultTap = type;
        console.log(this.defaultTap);
    }

    displayDate: any;
    year: any
    getBirthday(birthday: string) {
        let a = birthday.split(" ");
        var objDate = new Date(a[0]),
            locale = "en-us",
            month = objDate.toLocaleString(locale, { month: "long" });
        //let date = new Date(birthday);
        var dateString = birthday;
        var date = new Date(dateString.replace(' ', 'T'));
        this.displayDate = date.getDay();
        this.year = date.getFullYear();
        return this.getGetOrdinal(this.displayDate) + " " + month + " " + this.year;
    }
    getGetOrdinal(n) {
        var s = ["th", "st", "nd", "rd"],
            v = n % 100;
        return n + (s[(v - 20) % 10] || s[v] || s[0]);
    }

    editHours() {
        let editModal = this.modalCtrl.create(EditHours, { childData: this.name });
        editModal.present();
    }

    gotonotes() {
        this.productService.setSavedProducts(this.name.products);

        this.commandHandler.showLoader('GETTING PRODUCTS');

        var productObj = { date: moment().format('YYYY-MM-DD'), userId: this.userDetails.data.user_id, locationId: Number(localStorage.getItem('currentLocationId')) };

        if (this.network.type != 'none') {
            this.productService.getAllProducts(this.userDetails, productObj).then((data) => {
                this.commandHandler.hideLoader();
                if (localStorage.getItem('isDevice') === 'true') {
                    this.kidsDb.addProdcutListInDb(data, productObj.userId, productObj.locationId)
                }
                this.navCtrl.push(NotesPage, { products: data, childData: this.name });
            }, (data) => {
                this.commandHandler.hideLoader();
            });
        } else {
            this.commandHandler.hideLoader();

            if (localStorage.getItem('isDevice') === 'true') {
                this.kidsDb.getProdcutListInDb(String(productObj.locationId)).then((data) => {
                    if (data) {
                        data = JSON.parse(data[0].products);
                        this.navCtrl.push(NotesPage, { products: data, childData: this.name });
                    } else {
                        this.commandHandler.showAlert('Not Products Available', 'No products are available for this location. Please ON the internet to get the latest data!');
                    }
                    this.commandHandler.hideLoader();
                }, (data) => {
                    this.commandHandler.showAlert('Error', 'Something went wrong, Please try again!');
                });
            }
        }
    }

    goToRegister(name) {
        //console.log(JSON.parse(localStorage.getItem('locationData')));
        //if user try to update register time within a minute from last updated time
        if (name.selected && (name.child_time.h == 0 && name.child_time.m < 1)) {
            let childInOuts = this.productService.getChildInOuts(this.userDetails.data.user_id, name.child_id)[0]; 
            var hm = childInOuts.split(":");                      
            var min = Number(hm[1]);
            this.commandHandler.showAlert('Information', this.translate.instant('You can not deregister the child until after')+' '+ hm[0]+':'+((min+1)<10 ? '0'+(min+1):(min+1)));
        }
        else {
            var attemptsObj = this.kidList.saveUserAttempts(this.userDetails.data.user_id, name.child_id, moment().format('HH:mm'));
            console.log(attemptsObj);
            if (attemptsObj.attempts > Globals.ATTEMPTS) {
                this.commandHandler.showAlert('Information', 'You have already done maximum In and Out for the day.');
                name.selected = false;
            } else {

                if (name.selected == undefined) {
                    name.selected = true;
                    name.visited = false;
                } else if (name.selected) {
                    name.selected = false;
                    name.visited = true;
                } else {
                    name.selected = true;
                    name.visited = false;
                }

                console.log(name.selected);

                this.commandHandler.showLoader('LOADING');

                localStorage.setItem('currentChild', JSON.stringify({ id: name.child_id, value: name.selected, visited: name.visited }));

                var childObj = {
                    "user_id": this.userDetails.data.user_id,
                    "attendance_detail": {
                        "child": {
                            "date": moment().format('YYYY-MM-DD'),
                            "child_id": name.child_id,
                            "location_id": Number(localStorage.getItem('currentLocationId')),
                            "registration_id": name.registration_id,
                            "registration_day_id": null
                        }
                    }
                };

                childObj.attendance_detail.child[attemptsObj.attr] = moment().format('HH:mm');
                console.log(childObj);

                this.kidList.doRegistrationOfChild(this.userDetails, childObj).then((data) => {
                    this.commandHandler.hideLoader();
                }, (data) => {
                    // this.commandHandler.hideLoader();
                });
            }
        }

    }
}
