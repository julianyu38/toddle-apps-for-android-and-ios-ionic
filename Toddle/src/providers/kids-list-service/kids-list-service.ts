import { Injectable } from '@angular/core';
//import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import {Headers, Http} from '@angular/http';
import * as moment from 'moment/moment';
import {reject} from "q";
import {Globals} from "../../app/globals";
import {CommandHandler} from "../commandhandler";


/*
  Generated class for the KidsListServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class KidsListServiceProvider {
  url = 'https://mobile-kdv1.tactics.be/app.php/';
  constructor(public http: Http, public commandHandler : CommandHandler) {}
  getKidsList(userId:string, token:string, location:string, cityKey:string) {
    return new Promise((resolve, reject) => {
      var header = new Headers();

      header.append("city_key", cityKey);
      header.append("token", token);
      header.append('Content-Type', 'application/json');
      this.http.get(this.url +'location/'+ location +'/day/'+moment().format('YYYY-MM-DD')+'/user/' + userId + '/children', {headers: header})
        .map(res => res.json())
        .subscribe(data =>{
          resolve(data);
          },
          error => {
              reject(error);
              this.commandHandler.showAPIError(error);
          },
          () => console.log("Finished")
        );
    });
  }

  getTimeDifference = (sTime, eTime) => {
    var startTime = moment(sTime, 'hh:mm a');
    var endTime = moment(eTime, 'hh:mm a');

    var totalHours = (endTime.diff(startTime, 'hours'));
    var totalMinutes = endTime.diff(startTime, 'minutes');
    var clearMinutes = totalMinutes % 60;

    return { h : totalHours, m: clearMinutes };

  };

  calculateTotalTimes = (tempArr) => {
    var times = [];
    var totalTimes = {h : 0, m: 0, selected : false, visited: false};
    if (tempArr.length == 0) {
       return totalTimes;
    } else {

      if((tempArr.length%2) != 0) {
        totalTimes.selected = true
      } else {
          totalTimes.visited = true
      }

      for (var i = 0; i < tempArr.length; i++) {
        if (tempArr[i] != undefined) {
          if (tempArr[i + 1] != undefined) {
            times.push(this.getTimeDifference(tempArr[i], tempArr[i + 1]));
          } else {
            times.push(this.getTimeDifference(tempArr[i], moment().format('HH:mm')));
          }
          i = i + 1;
        }
      }

      times.forEach(function(item) {
        totalTimes.h = totalTimes.h + item.h;
        totalTimes.m = totalTimes.m + item.m;
      })

      if(totalTimes.m > 59) {
        var hours = Math.trunc(110/60);
        var minutes = 110 % 60;
        totalTimes.h = totalTimes.h + hours;
        totalTimes.m = minutes;
      }

      return totalTimes;

    }
  };

  getChildHours  = (userId, childId) => {
    var localChildData = JSON.parse(localStorage.getItem('childData'));
    if (localChildData != null) {

        var childIndex = null;

        localChildData.forEach(function (element, index) {
            if (element.cId == childId) {
                childIndex = index;
            }
        });

        if(childIndex != null) {
            console.log(childId);
            return this.calculateTotalTimes(localChildData[childIndex].values);
        } else {
            return { h: 0, m: 0, selected : false, visited: false};
        }
    } else {
        return { h: 0, m: 0, selected : false, visited: false};
    }
  }

  getCurrentChildsData = () => {
      var tempChildData = JSON.parse(localStorage.getItem('childData'));
      if(tempChildData == null) {
          var childData = [];
          localStorage.setItem('childData', JSON.stringify(childData));
      }

      return JSON.parse(localStorage.getItem('childData'));
  }

  saveAllUserTimes  = (userId, childsData) => {
      var localChildData = this.getCurrentChildsData();
      var userChildTimeArr = [];
      childsData.forEach(function (element) {
          var timeStrArr = [];
          if(element.hour_in_1 != '' && element.hour_in_1 != null) timeStrArr.push(element.hour_in_1);
          if(element.hour_out_1 != '' && element.hour_out_1 != null) timeStrArr.push(element.hour_out_1);

          if(element.hour_in_2 != '' && element.hour_in_2 != null) timeStrArr.push(element.hour_in_2);
          if(element.hour_out_2 != '' && element.hour_out_2 != null) timeStrArr.push(element.hour_out_2);

          if(element.hour_in_3 != '' && element.hour_in_3 != null) timeStrArr.push(element.hour_in_3);
          if(element.hour_out_3 != '' && element.hour_out_3 != null) timeStrArr.push(element.hour_out_3);

          if(element.hour_in_4 != '' && element.hour_in_4 != null) timeStrArr.push(element.hour_in_4);
          if(element.hour_out_4 != '' && element.hour_out_4 != null) timeStrArr.push(element.hour_out_4);


          if(timeStrArr.length > 0) {
              userChildTimeArr.push({uId : userId, cId: element.child_id, timesArr : timeStrArr});
          }
      })

      console.log(userChildTimeArr);

      userChildTimeArr.forEach(function (element, index) {

          var childIndex = 0;
          var isSameChild = false;
          localChildData.forEach(function(inElement, index) {
              if(inElement.cId  == element.cId) {
                  childIndex = index;
                  isSameChild = true;
              }
          });

          if(isSameChild) {
              localChildData[childIndex].values = element.timesArr;
          } else {
              localChildData.push({cId : element.cId, values: element.timesArr});
          }
      });

      localStorage.setItem('childData', JSON.stringify(localChildData));
  }

  saveUserAttempts  = (userId, childId, time) => {

    var localChildData = this.getCurrentChildsData();

    var isSameChild = false;
    var childIndex = 0;


    var timeArr = [];
    timeArr.push(time);

      localChildData.forEach(function(element, index) {
        if(element.cId  == childId) {
          childIndex = index;
          isSameChild = true;
        }
      });

      if(isSameChild) {
        localChildData[childIndex].values.push(time);
      } else {
        localChildData.push({cId : childId, values: timeArr});
        childIndex = localChildData.length - 1;
      }


    var attrType = '';
    var loadingContent = '';

    switch(localChildData[childIndex].values.length) {
      case 1 :
        attrType = 'hour_in1';
        loadingContent = 'Checked In';
        break;
      case 2 : attrType = 'hour_out1';
        loadingContent = 'Checked Out';
        break;
      case 3 : attrType = 'hour_in2';
        loadingContent = 'Checked In';
        break;
      case 4 : attrType = 'hour_out2';
        loadingContent = 'Checked Out';
        break;
      case 5 :
        attrType = 'hour_in3';
        loadingContent = 'Checked In';
        break;
      case 6 : attrType = 'hour_out3';
        loadingContent = 'Checked Out';
        break;
      case 7 : attrType = 'hour_in4';
        loadingContent = 'Checked In';
        break;
      case 8 : attrType = 'hour_out4';
        loadingContent = 'Checked Out';
        break;
    };

    // > Globals.ATTEMPTS
    if(localChildData[childIndex].values.length > Globals.ATTEMPTS) {
      return {attr: 'maxattempt', attempts : localChildData[childIndex].values.length, loadingContent: ''};
    } else {
      localStorage.setItem('childData', JSON.stringify(localChildData));
      return {attr: attrType, attempts : localChildData[childIndex].values.length, loadingContent:loadingContent};
    }
  }

  doRegistrationOfChild(userDetailObj, childObj) {
    return new Promise((resolve, reject) => {
      var header = new Headers();
      header.append("city_key", userDetailObj.installation_key);
      header.append("token", userDetailObj.data.token);
      header.append('Content-Type', 'application/json');
      var _url  = this.url + 'attendance/simple/registration';
      this.http.post(_url, childObj, {headers: header})
        .map(res => res.json())
        .subscribe(data =>{
            resolve(data);
          },
          error => {
              reject(error);
              if(error.status === 0) {
                  this.commandHandler.hideLoader();
                  this.commandHandler.updateToSyncQueue(_url, 'post', childObj);
              } else this.commandHandler.showAPIError(error);
          },
          () => console.log("Finished")
        );
    });
  }

  editRegistrationOfChild(userDetailObj, childObj) {
      return new Promise((resolve, reject) => {
          var header = new Headers();
          header.append("city_key", userDetailObj.installation_key);
          header.append("token", userDetailObj.data.token);
          header.append('Content-Type', 'application/json');
          var _url  = this.url + 'attendance/registration';
          this.http.post(this.url + 'attendance/registration', childObj, {headers: header})
              .map(res => res.json())
              .subscribe(data =>{
                      resolve(data);
                  },
                  error => {
                      reject(error);
                      if(error.status === 0) {
                          this.commandHandler.hideLoader();
                          this.commandHandler.updateToSyncQueue(_url, 'post', childObj);
                      } else this.commandHandler.showAPIError(error);
                  },
                  () => console.log("Finished")
              );
      });
  }
}
