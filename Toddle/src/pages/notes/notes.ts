import { Component } from '@angular/core';
import {PopoverController, ModalController, NavController, NavParams, LoadingController, Events} from "ionic-angular";
import { NotespopoverPage } from "../notespopover/notespopover";
import {ProductServiceProvider} from "../../providers/product-service/product-service";
import {TranslateService} from '@ngx-translate/core';
import {KidsListServiceProvider} from "../../providers/kids-list-service/kids-list-service";
import {Sqlite} from "../../providers/kids-database/sqlite";
import * as moment from 'moment/moment';
import {HotmealsnoPage} from "../hotmealsno/hotmealsno";
import {Network} from "@ionic-native/network";
import {Globals} from "../../app/globals";
import {CommandHandler} from "../../providers/commandhandler";

@Component({
  selector: 'page-notes',
  templateUrl: 'notes.html',
})
export class NotesPage {
    structure = {};
    products = [];
    childData:any;
    isEditState:Boolean = false;
    commentBox:any;
    placeText:any;
    childInOuts : any;
    userDetails: any;
    savedProducts: any;
    loader :any;
    tempChildList : any;
    constructor(public commandHandler: CommandHandler, public network: Network, public kidsDb: Sqlite,public kidList: KidsListServiceProvider, public translate: TranslateService, public events: Events, public loading: LoadingController, public productService: ProductServiceProvider, public popoverCtrl: PopoverController,public modalCtrl:ModalController,public navCtrl:NavController, public navParams: NavParams) {
        this.structure = {lower: 2, upper: 5}
        this.products = this.navParams.data.products;
        this.childData = this.navParams.data.childData;
        this.userDetails = JSON.parse(localStorage.getItem('userDetail'));
        this.childInOuts = this.productService.getChildInOuts(this.userDetails.data.user_id, this.childData.child_id);
        console.log(this.childInOuts);
        this.savedProducts = this.productService.getSavedProducts();

        console.log('this.savedProductsthis.savedProducts');
        console.log(this.savedProducts);
        console.log(this.products);

        if (!Globals.IS_NETWORK_UP) {
            this.concatOfflineProducts();
        } else {
            this.adjustMultiplePossible();
        }

        events.subscribe('productAdded', () => {
            this.commandHandler.showLoader('GETTING PRODUCT');
            var cProdStr = moment().format('YYYY-MM-DD')+"/"+this.userDetails.data.user_id+"/"+this.childData.registration_id;
            if (Globals.IS_NETWORK_UP) {
                this.productService.getCurrentChildProducts(this.userDetails, cProdStr).then((data) => {
                    this.tempChildList = data;
                    this.productService.setSavedProducts(this.tempChildList.products);
                    this.navParams.data.childData.products = this.tempChildList.products;
                    this.savedProducts = this.tempChildList.products;
                    this.adjustMultiplePossible();
                    this.commandHandler.hideLoader();
                });
            } else {
                this.concatOfflineProducts();
                this.commandHandler.hideLoader();
            }
        });

        this.savedProducts.forEach(function (element) {
            element.selected = false;
        });
    }

    ionViewDidLeave(){
        this.events.unsubscribe('productAdded');
    }

    concatOfflineProducts() {
        var offlineProds = JSON.parse(localStorage.getItem('newlyAddedProduct'));
        if(offlineProds != null) {
            var cProdArr = [];
            offlineProds.forEach(function (element) {
                if(element.child_id == this.childData.child_id) {
                    var isEditItem = false;
                    this.savedProducts.forEach(function (inElement) {
                        if(inElement.id == element.id) {
                            isEditItem = true;
                            inElement.quantity = element.quantity;
                        }
                    }.bind(this));

                    if(!isEditItem) cProdArr.push(element);
                }
            }.bind(this));

            this.savedProducts = this.productService.getSavedProducts().concat(cProdArr);
            this.productService.setSavedProducts(this.savedProducts);
        }

        this.adjustMultiplePossible();
    }

    adjustMultiplePossible() {
        this.savedProducts.forEach(function (element) {
            for(var i=0; i < this.products.length; i++) {
                if(element.name == this.products[i].name) {
                    element.multiple_possible = this.products[i].multiple_possible;
                    break;
                }
            }
        }.bind(this));
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad ProductScreen');
        if(this.childData.child_info == null) {
            this.placeText = this.translate.instant('ADD COMMENT');
        } else {
            this.placeText = this.translate.instant('EDIT COMMENT');
        }

        // for time being
        // this.events.publish('productAdded');
    }

    getImageURL() {
        return 'assets/child_profile_default.png';
    }

    onTapProduct(item) {
        this.savedProducts.forEach(function (element) {
            element.selected = false;
        });
        item.selected = true;
    }

    onCloseProduct(item) {
        item.selected = false;
    }

    onEditProduct(item) {
        console.log(item);
        item.selected = false;
        this.notesModal(false, item)
    }

    onDeleteProduct(item) {
        var pId = (item.product_id !== undefined) ? item.product_id : item.id;
        var delObj = {
            "product_id" : pId,
            "registration_day_id" : this.childData.registration_day_id
        };

        console.log(delObj);
        if (!Globals.IS_NETWORK_UP) {
            let delIndex = undefined;
            this.savedProducts.forEach(function (element, index) {
                if(element.id == pId) {
                    delIndex = index;
                }
            }.bind(this));

            this.savedProducts.splice(delIndex, 1);
            this.productService.setSavedProducts(this.savedProducts);

            this.productService.deleteProductIntoQueue(this.userDetails.data.user_id, delObj);

        } else {
            this.commandHandler.showLoader('DELETE PRODUCT');
            this.productService.deleteProduct(this.userDetails,delObj).then((data) => {
                this.commandHandler.hideLoader();
                this.events.publish('productAdded');
            },(data) => {
                this.commandHandler.hideLoader();
            });
        }

    }


    notesModal(isNew, data) {

        if(!isNew) {
            localStorage.setItem('editItem', JSON.stringify(data));
            let quantityModal = this.modalCtrl.create(HotmealsnoPage, {product: data, childData: this.childData});
            quantityModal.present();
        } else {
            localStorage.removeItem(('editItem'));
            localStorage.setItem('regDayId', this.childData.registration_day_id);
            localStorage.setItem('regId', this.childData.registration_id);
            let profileModal = this.modalCtrl.create(NotespopoverPage, {products : this.products, childData: this.childData});
            profileModal.present();
        }
    }

    onTapCommentEdit() {
        this.placeText = this.translate.instant('EDIT COMMENT');
        this.isEditState = true;
        this.commentBox = this.childData.child_info;
    }

    onTapCommentAdd() {
        if(this.commentBox != undefined && this.commentBox != '') {
            this.isEditState = false;
            var commentObj = {child_id : this.childData.child_id, comment: this.commentBox};
            this.commandHandler.showLoader('SAVING COMMENT');
            this.productService.saveChildComments(this.userDetails,commentObj).then((data) => {
                this.childData.child_info = this.commentBox;
                this.commandHandler.hideLoader();
            },(data) => {
                this.childData.child_info = this.commentBox;
                this.commandHandler.hideLoader();
            });
        }
    }

    onTapDeleteComment() {
        this.isEditState = false;
        var commentObj = {child_id : this.childData.child_id, comment: ''};
        this.commandHandler.showLoader('DELETE COMMENT');
        this.productService.saveChildComments(this.userDetails,commentObj).then((data) => {
            this.childData.child_info = null;
            this.commentBox = '';
            this.commandHandler.hideLoader();
        },(data) => {
            this.childData.child_info = null;
            this.commentBox = '';
            this.commandHandler.hideLoader();
        });
    }

    goBack() {
        this.navCtrl.pop();
    }
}
  
