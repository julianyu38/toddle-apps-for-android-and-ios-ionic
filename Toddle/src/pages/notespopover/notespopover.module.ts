import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NotespopoverPage } from './notespopover';

@NgModule({
  declarations: [
    NotespopoverPage,
  ],
  imports: [
    IonicPageModule.forChild(NotespopoverPage),
  ],
  exports: [
    NotespopoverPage
  ]
})
export class NotespopoverPageModule {}
