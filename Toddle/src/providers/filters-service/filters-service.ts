import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import {TranslateService} from '@ngx-translate/core';

/*
  Generated class for the FiltersServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class FiltersServiceProvider {
    groups:any;
    planned:any;
    schedule:any;
    popupType:any;
  constructor(public http: Http,public translate: TranslateService) {

  }

  getListOfSelectedLabels() {
      let labelArr = [];

      var cAge = JSON.parse(localStorage.getItem('age'));
      if(cAge == null || cAge == 'undefined') {
          cAge = {lower: 0, upper: 6};
      };

      if(cAge.lower == 0 && cAge.upper == 6) {
          // To Do
      } else {
          labelArr.push(this.translate.instant('AGE')+' '+cAge.lower+ ' - '+cAge.upper);
      }

      let isMale = localStorage.getItem('male') == 'true' ? true : false;
      let isFemale = localStorage.getItem('female') == 'true' ? true : false;
      if(localStorage.getItem('male') == null || localStorage.getItem('male') == 'undefined') {
          isMale = true;
      }

      if(localStorage.getItem('female') == null || localStorage.getItem('female') == 'undefined') {
          isFemale = true;
      }

      var filterGender = '';
      if(isMale && isFemale) {
          // filterGender = 'All Gender';
      } else {
          if(isMale) {
              filterGender = this.translate.instant('MALE');
          } else if(isFemale) {
              filterGender = this.translate.instant('FEMALE');
          }
      }

      if(filterGender != '') labelArr.push(filterGender);

      if( localStorage.getItem('savedGroups') == null ||  localStorage.getItem('savedGroups') == 'undefined') {
          // labelArr.push('All Groups');
      } else {
          var sGroups = JSON.parse(localStorage.getItem('savedGroups'));

          if(sGroups[0].selected == true) {
              // labelArr.push('All Groups');
          } else {
              for(let i=0; i < sGroups.length; i++) {
                  if(sGroups[i].selected == true) {
                      labelArr.push(sGroups[i].child_group_name);
                  }
              }
          }

      }

      if( localStorage.getItem('savedPlanned') == null ||  localStorage.getItem('savedPlanned') == 'undefined') {
          // labelArr.push('Planned');
          // labelArr.push('Unplanned');
      } else {
          var sPlanned = JSON.parse(localStorage.getItem('savedPlanned'));

          if(sPlanned[0].selected == true) {
             // labelArr.push('Planned');
             // labelArr.push('Unplanned');
          } else {
              for(let i=0; i < sPlanned.length; i++) {
                  if(sPlanned[i].selected == true) {
                      labelArr.push(sPlanned[i].child_group_name);
                  }
              }
          }
      }

      if( localStorage.getItem('savedSchedule') == null ||  localStorage.getItem('savedSchedule') == 'undefined') {
          // labelArr.push('All Schedule');
      } else {
          var sSchedule = JSON.parse(localStorage.getItem('savedSchedule'));

          if(sSchedule[0].selected == true) {
              // labelArr.push('All Schedule');
          } else {
              for(let i=0; i < sSchedule.length; i++) {
                  if(sSchedule[i].selected == true) {
                      labelArr.push(sSchedule[i].child_group_name);
                  }
              }
          }
      }

      return labelArr;
  }
  //get filter age
  getFilters():any{
    return JSON.parse('age');
  }

  // set gender of child to filter data
  setFilter(age, male, female){
    localStorage.setItem('age', JSON.stringify(age));
    localStorage.setItem('male', male);
    localStorage.setItem('female', female);
  }

    setCurPopup(type) {
        this.popupType = type;
    }

    setPopupData(data) {
        switch (this.popupType) {
            case 'group' : this.groups = data;
                break;
            case 'planned' : this.planned = data;
                break;
            case 'schedule' : this.schedule = data;
                break;
        }
    }

    resetAllPopupData(data) {
        this.groups = data;
        this.planned = data;
        this.schedule = data;
    }

    getTitleForPopUp() {
        var title = '';
        switch (this.popupType) {
            case 'group' : title = this.translate.instant('SELECT_GROUPS');
                break;
            case 'planned' : title = this.translate.instant('SELECT_PLANNED');
                break;
            case 'schedule' : title = this.translate.instant('SELECT_SCHEDULE');
                break;
        }

        return title;
    }

    storeDataInLocal(key, data) {
        localStorage.setItem(key, JSON.stringify(data));
    }

    getPopupData(type) {
        var data;
        switch (type) {
            case 'group' : data = this.groups;
                break;
            case 'planned' : data = this.planned;
                break;
            case 'schedule' : data = this.schedule;
                break;
        }

        return data;
    }


  //get filter list from popover
  filterData(data:any) {

      var age = JSON.parse(localStorage.getItem('age'));
      var isMale = localStorage.getItem('male') == 'true' ? true : false;
      var isFemale = localStorage.getItem('female') == 'true' ? true : false;

      var filterGender = ''
      if(isMale && isFemale || (!isMale && !isFemale)) {
          filterGender = 'All';
      } else {
          if(isMale) {
              filterGender = 'M';
          } else if(isFemale) {
              filterGender = 'V';
          }
      }


    var planned;

    var genderArray = [];
    data.forEach(function (element) {
        if(element.child_gender == filterGender || filterGender == 'All') {
            genderArray.push(element);
        }
    });

    var ageArray = [];

    if(age == null || age == undefined || age == 'undefined') {
        ageArray = genderArray;
    } else {
        genderArray.forEach(function (element) {
            if(element.child_age >= age.lower && element.child_age <= age.upper) {
                ageArray.push(element);
            }
        });
    }


    var plannedArray = [];
    if(localStorage.getItem('savedPlanned') != null && localStorage.getItem('savedPlanned') != 'undefined') {
        planned = JSON.parse(localStorage.getItem('savedPlanned'));
        var isAllPlan = planned[0].selected;
        var isPlanned = planned[1].selected;
        var isUnplanned = planned[2].selected;

        if(isAllPlan == true || (isPlanned && isUnplanned)) {
            plannedArray = ageArray;
        } else {
            if(isPlanned == true) {
                ageArray.forEach(function (element) {
                    if(element.planned == 1) {
                        plannedArray.push(element);
                    }
                });
            } else if(isUnplanned == true) {
                ageArray.forEach(function (element) {
                    if(element.planned != 1) {
                        plannedArray.push(element);
                    }
                });
            } else {
                // TO DO
                plannedArray = ageArray;
            }
        }
    } else {
        plannedArray = ageArray;
    }


    var groupsArray = [];
    if(localStorage.getItem('savedGroups') != null && localStorage.getItem('savedGroups') != 'undefined') {
        var savedG = JSON.parse(localStorage.getItem('savedGroups'));
        if(savedG[0].selected == true) {
            groupsArray = plannedArray;
        } else {
            var ids = [];
            savedG.forEach(function(ele) {
                if(ele.selected) {
                    ids.push(ele.child_group_id);
                }
            });

            plannedArray.forEach(function (element) {
                ids.forEach(function (inElement) {
                    if(inElement == element.child_group_id) {
                        groupsArray.push(element);
                    }
                });
            });
        }
    } else {
        groupsArray = plannedArray;
    }


      var scheduleArray = [];
      if(localStorage.getItem('savedSchedule') != null && localStorage.getItem('savedSchedule') != 'undefined') {
          var savedSch = JSON.parse(localStorage.getItem('savedSchedule'));
          console.log(savedSch);
          if(savedSch[0].selected == true) {
              scheduleArray = groupsArray;
          } else {
              /*if(savedSch[1].selected == true) {
                  groupsArray.forEach(function (element) {
                        console.log(element);
                        if(element.selected) {
                            scheduleArray.push(element);
                        }
                  });
              } else {
                  scheduleArray = groupsArray;
              }*/

              groupsArray.forEach(function (element) {
                  if(element.selected && savedSch[1].selected == true) {
                      scheduleArray.push(element);
                  } else if(element.visited && savedSch[2].selected == true) {
                      scheduleArray.push(element);
                  } else if(!element.selected && !element.visited && savedSch[3].selected == true) {
                      scheduleArray.push(element);
                  }
              });
          }
      } else {
          scheduleArray = groupsArray;
      }

    return scheduleArray;

  }
}
